# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

#

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from .Debug import debug
from .Tweet import Tweet
from .Column import Column, OrderedColumn

class StreamView(QWidget):
    unread_signal = pyqtSignal(int)
    def __init__(self, window):
        QWidget.__init__(self)
        self.window = window

        self.layout = QHBoxLayout()
        self.setLayout(self.layout)
        self.new_tweets = OrderedColumn(self.window)
        self.layout.addWidget(self.new_tweets)
        self.old_tweets = Column(self.window)
        self.layout.addWidget(self.old_tweets)

        self.new_tweets.read_tweets.connect(self.read_tweets)
        self._unread_count = 0

    @property
    def unread_count(self):
        return self._unread_count

    @unread_count.setter
    def unread_count(self, val):
        self._unread_count = val
        self.unread_signal.emit(val)

    def add_tweet(self, tweet):
        self.new_tweets.add_tweet(tweet)
        QApplication.alert(self)
        self.unread_count += 1

    read_tweet = pyqtSignal(str)
    def read_tweets(self, n):
        for i in range(n):
            tweet = self.new_tweets.remove_tweet()
            if tweet:
                self.old_tweets.add_tweet(tweet)
                self.read_tweet.emit(tweet.tweet.id_str)
                self.unread_count -= 1
                if tweet.expanded:
                    tweet.toggle_expand()
                    if self.new_tweets.tweets:
                        next_tweet = self.new_tweets.tweets[-1]
                        if not next_tweet.expanded:
                            next_tweet.toggle_expand()

    def remove_tweet(self, tweet):
        t = self.new_tweets.remove_tweet(tweet)
        if not t:
            t = self.old_tweets.remove_tweet(tweet)
        else:
            self.unread_count -= 1
        if t:
            t.hide()
