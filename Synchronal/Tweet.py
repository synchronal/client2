# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

import functools
import os
from collections import namedtuple, Counter
import re

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtNetwork import *
from PyQt5.QtWidgets import *

from .Debug import debug
from .RequestHeaders import RequestHeaders
from .Tweeter import Tweeter

class Tweet(QFrame):
    default_margins = []
    stats = Counter()
    def __init__(self, tweet, window, *args, embed=False):
        QFrame.__init__(self, *args)
        self.tweet = tweet
        self.window = window
        self.expanded = False
        self.has_embed = False
        self.is_embed = embed

        self.media_requests = []
        self.media_items = {}
        self.media_entities = []
        self.media_try_count = 8
        self.media_widget = None
        self.tweeter = None
        # 96 is the "standard" dpi
        scale = QApplication.instance().desktop().logicalDpiX() / 96

        main_layout = QVBoxLayout()
        self.setLayout(main_layout)

        self.favorited = self.tweet.favorited
        self.retweeted = self.tweet.retweeted

        if "retweeted_status" in self.tweet:
            p = self.tweet.retweeted_status
        else:
            p = self.tweet

        header_layout = QHBoxLayout()
        main_layout.addLayout(header_layout)

        # username row
        user = QLabel()
        color = user.palette().text().color().name()
        self.plain_link_style = "color: {0}; text-decoration: none;".format(color)
        userstr = '<strong><a href="https://twitter.com/{0}" style="{2}">{0}</a></strong> ({1})'.format(p.user.screen_name, p.user.name, self.plain_link_style)
        if "retweeted_status" in self.tweet:
            userstr += " [RT by {}]".format(self.tweet.user.screen_name)
        if "in_reply_to_screen_name" in p and p.in_reply_to_screen_name:
            userstr += " [Reply to {}]".format(p.in_reply_to_screen_name)
        user.setText(userstr)
        user.setWordWrap(True)
        user.setOpenExternalLinks(True)
        user.setTextInteractionFlags(Qt.LinksAccessibleByMouse)
        header_layout.addWidget(user)
        if not embed:
            self.stats[self.tweet.user.screen_name] += 1

        # expander button
        if not embed:
            self.expander = QLabel()
            self.expander.setTextInteractionFlags(Qt.LinksAccessibleByMouse)
            self.expander.linkActivated.connect(self.toggle_expand)
            self.expander.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
            width = self.expander.fontMetrics().boundingRect("[++]").width()
            # totally arbitrary padding
            self.expander.setMaximumWidth(width + 2)
            self.expander.setAlignment(Qt.AlignTop | Qt.AlignRight)
            header_layout.addWidget(self.expander)

        content_layout = QHBoxLayout()
        main_layout.addLayout(content_layout)

        # profile image
        self.img_label = QLabel()
        if embed:
            self.profile_image_size = int(32 * scale)
        else:
            self.profile_image_size = int(48 * scale)
        self.img_label.setMinimumHeight(self.profile_image_size)
        self.img_label.setMaximumHeight(self.profile_image_size)
        self.img_label.setMinimumWidth(self.profile_image_size)
        self.img_label.setMaximumWidth(self.profile_image_size)
        content_layout.addWidget(self.img_label)
        self.get_profile_img()

        # entity handling here
        processed_text = self.handle_entities(p)
        # main tweet text
        text_label = QLabel(processed_text)
        text_label.setTextFormat(Qt.RichText)
        text_label.setWordWrap(True)
        text_label.setTextInteractionFlags(Qt.LinksAccessibleByMouse | Qt.TextSelectableByMouse)
        text_label.setOpenExternalLinks(True)
        content_layout.addWidget(text_label)

        if not embed:
            self.extras_widget = QWidget()
            main_layout.addWidget(self.extras_widget)
            self.extras_layout = QVBoxLayout()
            self.extras_widget.setLayout(self.extras_layout)

            # quoted status
            if "quoted_status" in p and p.quoted_status:
                quoted_status_widget = Tweet(p.quoted_status, window, embed=True)
                self.extras_layout.addWidget(quoted_status_widget)
                self.has_embed = True

            # timestamp
            timestamp = QDateTime(p.created_at)
            timestamp.setTimeSpec(Qt.UTC)
            timestamp = timestamp.toLocalTime()
            time_label = QLabel('<a href="https://twitter.com/{}/status/{}" style="{}">{}</a>'.format(p.user.screen_name, p.id_str, self.plain_link_style, timestamp.toString(Qt.SystemLocaleShortDate)))
            time_label.setTextInteractionFlags(Qt.LinksAccessibleByMouse)
            time_label.setOpenExternalLinks(True)
            self.extras_layout.addWidget(time_label)

            # reply button
            self.reply_layout = QHBoxLayout()
            self.extras_layout.addLayout(self.reply_layout)
            font = QFont("Font Awesome 5 Free")
            self.reply_button = QToolButton()
            self.reply_button.setFont(font)
            self.reply_button.setToolButtonStyle(Qt.ToolButtonTextOnly)
            self.reply_button.setText("\uf3e5")
            self.reply_button.clicked.connect(self.prepare_reply)
            self.reply_layout.addWidget(self.reply_button)

            self.quote_button = QToolButton()
            self.quote_button.setFont(font)
            self.quote_button.setToolButtonStyle(Qt.ToolButtonTextOnly)
            self.quote_button.setText("\uf10e")
            self.quote_button.clicked.connect(self.prepare_quote)
            self.reply_layout.addWidget(self.quote_button)

            # retweet/like buttons

            self.retweet_button = QToolButton()
            self.retweet_button.setCheckable(True)
            self.retweet_button.setChecked(self.retweeted)
            self.retweet_button.setFont(font)
            self.retweet_button.setToolButtonStyle(Qt.ToolButtonTextOnly)
            self.retweet_button.setText("\uf079")
            self.retweet_button.clicked.connect(self.do_retweet)
            self.reply_layout.addWidget(self.retweet_button)

            self.favorite_button = QToolButton()
            self.favorite_button.setCheckable(True)
            self.favorite_button.setChecked(self.favorited)
            self.favorite_button.setFont(font)
            self.favorite_button.setToolButtonStyle(Qt.ToolButtonTextOnly)
            self.favorite_button.setText("\uf004")
            self.favorite_button.clicked.connect(self.do_favorite)
            self.reply_layout.addWidget(self.favorite_button)

            self.update_expansion()
        else:
            self.enable_frame(True)

    # number of characters is totally arbitrary
    # TODO determine this based on actual width of the column and max
    # character width
    wrap_length = 35
    wrap_pattern = re.compile(r"(?:(?<=[\s-])|^)[^\s-]+(?=[\s-]|$)")
    def handle_entities(self, tweet):
        out_entities = set()
        self.quick_link = None
        extended_entities = False
        if "extended_tweet" in tweet:
            dis_start, dis_end = tweet.extended_tweet.display_text_range
            text = tweet.extended_tweet.full_text
            extended_entities = tweet.extended_tweet.extended_entities if "extended_entities" in tweet else None
            entities = tweet.extended_tweet.entities if "entities" in tweet.extended_tweet else None
        else:
            if "full_text" in tweet:
                dis_start, dis_end = tweet.display_text_range
                text = tweet.full_text
            else:
                dis_start, dis_end = 0, len(tweet.text)
                text = tweet.text
            extended_entities = tweet.extended_entities if "extended_entities" in tweet else None
            entities = tweet.entities if "entities" in tweet else None
        if extended_entities:
            if "media" in extended_entities:
                for entity in extended_entities["media"]:
                    out_entities.add(self.handle_entity(entity, "media"))
        if entities:
            etypes = ("urls", "user_mentions")
            if not extended_entities:
                etypes = ("media",) + etypes
            for etype in etypes:
                if etype in entities:
                    for entity in entities[etype]:
                        out_entities.add(self.handle_entity(entity, etype))
        # break long words
        # this has to be done in the same step as entities to avoid index issues
        for m in self.wrap_pattern.finditer(text):
            g = m.group()
            if len(g) > self.wrap_length:
                out_entities.add(BrokenWord(
                    (m.start(), m.end()),
                    "- ".join(g[i:i+self.wrap_length] for i in range(0, len(g), self.wrap_length))
                    ))
        out_entities = sorted(out_entities)
        if len(out_entities) > 1:
            drop = []
            for i in range(1, len(out_entities)):
                if out_entities[i-1].indices[1] > out_entities[i].indices[0]:
                    # overlapping entities - probably one is a wrapped word
                    # this shouldn't happen, but...
                    debug("Entity overlap:", out_entities[i-1], out_entities[i])
                    if isinstance(out_entities[i], BrokenWord):
                        drop.append(i)
                    else:
                        drop.append(i-1)
            for i in reversed(drop):
                del out_entities[i]
        out_text = []
        lasti = 0

        # trim out hidden entities
        trimmed_entities = []
        for e in out_entities:
            if e.indices[1] <= dis_start:
                continue
            if e.indices[0] >= dis_end:
                continue
            indices = (e.indices[0] - dis_start, e.indices[1] - dis_start)
            trimmed_entities.append(Entity(indices, e.text))
        text = text[dis_start:dis_end]

        for e in trimmed_entities:
            out_text.append(text[lasti:e.indices[0]] + e.text)
            lasti = e.indices[1]
        out_text.append(text[lasti:])
        return "".join(out_text)

    def handle_entity(self, entity, etype):
        if etype in ("media", "urls"):
            d_url = entity.display_url
            if len(d_url) > self.wrap_length:
                # wrap long urls
                d_url = " ".join(d_url[i:i+self.wrap_length] for i in range(0,len(d_url),self.wrap_length))
            text = '<a href="{}">{}</a>'.format(entity.expanded_url, d_url)
        if etype == "media":
            self.has_embed = True
            if not hasattr(self, "media_url"):
                self.media_entities.append(entity)
                self.get_media(entity)
            if self.quick_link is None:
                # there's only ever one media url even if there's multiple media entities
                self.quick_link = (entity.expanded_url, "media")
        elif etype == "urls":
            if (self.quick_link is None) or (self.quick_link[1] == "media"):
                self.quick_link = (entity.expanded_url, "link")
        elif etype == "user_mentions":
            text = '<a href="https://twitter.com/{0}">@{0}</a>'.format(entity.screen_name)
        return Entity(tuple(entity.indices), text)

    def get_profile_img(self):
        if "retweeted_status" in self.tweet:
            url = self.tweet.retweeted_status.user.profile_image_url_https
        else:
            url = self.tweet.user.profile_image_url_https
        # to avoid scaled images looking fugly, remove the thing that makes us
        # get the 32*32 pixel version
        url = url.replace("_normal", "")
        # pull file extension here so the format is detected, which avoids
        # problems with mislabled files
        path = os.path.splitext(QUrl(url).path().replace("/", "_"))[0]
        cache = QStandardPaths.writableLocation(QStandardPaths.CacheLocation)
        if not os.path.exists(cache):
            os.makedirs(cache)
        self.img_path = os.path.join(cache, path)
        if path in os.listdir(cache):
            self.set_profile_img(QPixmap(self.img_path))
        else:
            twitter = self.window.twitter
            try:
                reqs = twitter.img_requests
            except AttributeError:
                twitter.img_requests = reqs = {}
            if url in reqs:
                # once the owner of that request has completed it, retry
                # get_profile_img to pull the image from the disk cache
                reqs[url].finished.connect(self.get_profile_img)
            else:
                req = RequestHeaders(QUrl(url))
                self.profile_img_request = twitter.manager.get(req)
                self.profile_img_request.finished.connect(self.got_profile_img)
                reqs[url] = self.profile_img_request

    def set_profile_img(self, pixmap):
        if pixmap.isNull():
            debug("Null pixmap received")
            return
        self.img_label.setPixmap(pixmap.scaled(self.profile_image_size, self.profile_image_size, Qt.KeepAspectRatio, Qt.SmoothTransformation))

    def got_profile_img(self):
        if self.profile_img_request.error() != QNetworkReply.NoError:
            debug("Image request error:", self.profile_img_request.error())
            debug(self.profile_img_request.url())
            debug(self.profile_img_request.readAll().data())
            return
        data = self.profile_img_request.readAll()
        del self.profile_img_request
        with open(self.img_path, "wb") as f:
            f.write(data.data())
        pixmap = QPixmap()
        pixmap.loadFromData(data)
        self.set_profile_img(pixmap)

    def get_media(self, entity):
        # no need to cache this
        # we grab the largest size and scale to suit later
        req = RequestHeaders(QUrl(entity.media_url_https + ":large"))
        res = self.window.twitter.manager.get(req)
        self.media_requests.append((res, entity))
        res.finished.connect(self.got_media)

    def got_media(self):
        for i in self.media_requests[:]:
            res, entity = i
            if not res.isFinished():
                continue
            self.media_requests.remove(i)
            err = res.error()
            if err == QNetworkReply.ContentNotFoundError:
                # sometimes it takes a moment for the image to become available
                # wait 30 seconds and try again, up to a limit of four tries
                self.media_try_count -= 1
                if self.media_try_count <= 0:
                    debug("Persistent media 404 errors")
                    return
                QTimer.singleShot(30*1000, functools.partialmethod(self.get_media, entity))
                return
            elif err != QNetworkReply.NoError:
                debug("Media request error:", err)
                debug(res.url())
                debug(res.readAll().data())
                return
            data = res.readAll()
            pixmap = QPixmap()
            pixmap.loadFromData(data)
            self.media_items[self.media_entities.index(entity)] = pixmap
            if self.media_widget is None:
                self.media_widget = MediaWidget(self, entity.type, entity.expanded_url)
                if not self.is_embed:
                    self.extras_layout.insertWidget(0, self.media_widget)
                else:
                    self.layout().addWidget(self.media_widget)
                    self.media_widget.update_image()
            else:
                self.media_widget.image_added()
            if self.expanded:
                self.media_widget.update_image()

    def prepare_quote(self):
        self.do_reply(True)

    def prepare_reply(self):
        self.do_reply(False)

    @debug
    def do_reply(self, quote):
        if self.tweeter:
            self.tweeter.hide()
            self.tweeter.deleteLater()
        self.tweeter = Tweeter(self.window.twitter, reply_id=self.tweet.id_str, reply_name=self.tweet.user.screen_name, quote=quote)
        self.extras_layout.addWidget(self.tweeter)
        self.tweeter.posted.connect(self.done_reply)
        self.tweeter.input_box.setFocus()

    def done_reply(self, success):
        if success:
            self.tweeter.hide()
            self.tweeter.deleteLater()
            self.tweeter = None

    def toggle_expand(self):
        if self.expanded:
            self.expanded = False
        else:
            self.expanded = True
        self.update_expansion()

    def enable_frame(self, enabled):
        if not self.default_margins:
            # we need to work this out
            self.setFrameStyle(QFrame.StyledPanel | QFrame.Raised)
            self.default_margins.append(self.contentsMargins())
        if enabled:
            self.setFrameStyle(QFrame.StyledPanel | QFrame.Raised)
            self.setContentsMargins(self.default_margins[0])
        else:
            self.setFrameStyle(QFrame.NoFrame | QFrame.Plain)
            self.setContentsMargins(self.default_margins[0])

    def update_expansion(self):
        self.enable_frame(self.expanded)
        if self.expanded:
            self.extras_widget.setVisible(True)
            if self.media_widget:
                self.media_widget.update_image()
            ex_state = "-"
        else:
            self.extras_widget.setVisible(False)
            if self.has_embed:
                ex_state = "++"
            else:
                ex_state = "+"
        self.expander.setText('<a href="synchronal://expand" style="{}">[{}]</a>'.format(self.plain_link_style, ex_state))

    def do_retweet(self, checked):
        if checked:
            self.window.twitter.statuses.retweet(self.tweet.id_str).callback(self.done_retweet)
        else:
            self.window.twitter.statuses.unretweet(self.tweet.id_str).callback(self.done_retweet)
        self.retweet_button.setEnabled(False)

    def done_retweet(self, res):
        self.retweet_button.setEnabled(True)
        if res is None:
            # error
            self.retweet_button.setChecked(self.retweeted)
            return
        self.retweeted = self.retweet_button.isChecked()

    def do_favorite(self, checked):
        if checked:
            self.window.twitter.favorites.create(self.tweet.id_str).callback(self.done_favorite)
        else:
            self.window.twitter.favorites.destroy(self.tweet.id_str).callback(self.done_favorite)
        self.favorite_button.setEnabled(False)

    def done_favorite(self, res):
        self.favorite_button.setEnabled(True)
        if res is None:
            # error
            self.favorite_button.setChecked(self.favorited)
            return
        self.favorited = self.retweet_button.isChecked()

    def __lt__(self, other):
        ts = self.tweet.created_at < other.tweet.created_at
        tid = int(self.tweet.id_str) < int(other.tweet.id_str)
        if ts != tid:
            debug("timestamp/id ordering mismatch: {} {} {} {}".format(self.tweet.created_at, other.tweet.created_at, self.tweet.id_str, other.tweet.id_str))
        return tid
    def __le__(self, other):
        ts = self.tweet.created_at <= other.tweet.created_at
        tid = int(self.tweet.id_str) <= int(other.tweet.id_str)
        if ts != tid:
            debug("timestamp/id ordering mismatch: {} {} {} {}".format(self.tweet.created_at, other.tweet.created_at, self.tweet.id_str, other.tweet.id_str))
        return tid


Entity = namedtuple("Entity", "indices text")
BrokenWord = namedtuple("BrokenWord", "indices text")


class MediaWidget(QWidget):
    def __init__(self, tweet, mtype, link, *args):
        super().__init__(*args)
        self.tweet = tweet
        self.mtype = mtype
        self.link = link
        self.n_images = 1
        self.stepper = None
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.image_label = QLabel()
        self.layout.addWidget(self.image_label)
        self.index = sorted(self.tweet.media_items.keys())[0]
        self.stepper = QLabel()
        self.stepper.linkActivated.connect(self.handle_link)
        self.layout.addWidget(self.stepper)
        self.update_stepper()

    def image_added(self):
        self.update_stepper()
        self.n_images += 1

    def update_stepper(self):
        if self.mtype in ("animated_gif", "movie", "video"):
            fill = "Play"
        elif self.mtype == "photo":
            k = sorted(self.tweet.media_items.keys())[-1] + 1
            fill = "{}/{}".format(self.index+1, k)
        else:
            fill = "?"
            debug("Unknown media type {}".format(self.mtype))
        text = '<a href="synchronal://prev" style="{0}">&lt;</a> <a href="{1}" style="{0}">{2}</a> <a href="synchronal://next" style="{0}">&gt;</a>'.format(self.tweet.plain_link_style, self.link, fill)
        self.stepper.setText(text)

    def handle_link(self, link):
        if "synchronal://" in link:
            self.step(link)
        else:
            QDesktopServices.openUrl(QUrl(link))

    def step(self, link):
        k = sorted(self.tweet.media_items.keys())
        i = k.index(self.index)
        if "next" in link:
            i += 1
        else:
            i -= 1
        if i >= len(k):
            i = 0
        elif i < 0:
            i = len(k) - 1
        self.index = k[i]
        self.update_image()
        self.update_stepper()

    def update_image(self):
        pm = self.tweet.media_items[self.index]
        # offset from maximum width and the maximum height are fundamentally arbitrary
        # width offset is a guess at scroll bar width, frame size, plus a bit
        # TODO this should update automatically if visible when the window is resized
        # this lookup is ugly, but we don't want it to resize itself huge if the tweet is huge etc
        width = self.tweet.window.stream_view.new_tweets.width() - 100
        scale = QApplication.instance().desktop().logicalDpiX() / 96
        height = int(400 * scale)
        pm = pm.scaled(width, height, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.image_label.setPixmap(pm)


class Stats(QScrollArea):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        text = "\n".join("{}: {}".format(*i) for i in Tweet.stats.most_common())
        self.label = QLabel(text)
        self.setWidget(self.label)
