# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

import re

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

#

class Tweeter(QWidget):
    posted = pyqtSignal(bool)
    def __init__(self, twitter, reply_id=None, reply_name=None, quote=False):
        QWidget.__init__(self)
        self.twitter = twitter

        layout = QHBoxLayout()
        self.setLayout(layout)

        self.input_box = QLineEdit()
        layout.addWidget(self.input_box)
        self.input_box.setPlaceholderText("Tweet")
        self.input_box.returnPressed.connect(self.post_tweet)
        self.input_box.textChanged.connect(self.set_length)

        self.counter = QLabel("0")
        layout.addWidget(self.counter)
        self.msg_length = 0
        self.quote = quote

        if reply_id and reply_name:
            if quote:
                self.input_box.setPlaceholderText("Quote tweet @{}".format(reply_name))
            else:
                self.input_box.setText("@{} ".format(reply_name))
                self.input_box.setPlaceholderText("Reply to @{}".format(reply_name))
            self.reply_id = reply_id
            self.reply_name = reply_name
        else:
            self.reply_id = None

    def post_tweet(self):
        if self.msg_length > 280:
            QMessageBox.warning(self, "Synchronal", "Tweets must be 280 characters or less.")
            return
        text = self.input_box.text()
        if self.quote:
            text += " https://twitter.com/{}/status/{}".format(self.reply_name, self.reply_id)
        self.twitter.statuses.update(status=text, in_reply_to_status_id=self.reply_id).callback(self.posted_callback)
        self.input_box.setEnabled(False)

    def posted_callback(self, res):
        self.input_box.setEnabled(True)
        if res is None:
            # error
            self.posted.emit(False)
        else:
            self.input_box.setText("")
            self.posted.emit(True)

    def set_length(self, text):
        length = len(text)
        # parse URLs which will be wrapped by t.co
        for i in re.findall(r"http://\S+", text):
            length -= len(i) - 22
        for i in re.findall(r"https://\S+", text):
            length -= len(i) - 23

        if length > 280:
            self.counter.setText('<span style="color:red">{0}</span>'.format(str(length)))
        else:
            self.counter.setText(str(length))
        self.msg_length = length
