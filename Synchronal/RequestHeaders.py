# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

#

from PyQt5.QtCore import *
from PyQt5.QtNetwork import *

from Synchronal import user_agent

class RequestHeaders(QNetworkRequest):
    def __init__(self, url=QUrl(), post=False):
        QNetworkRequest.__init__(self, url)
        self.setRawHeader(b"User-Agent", user_agent)
        if post:
            self.setHeader(QNetworkRequest.ContentTypeHeader, "application/x-www-form-urlencoded")
