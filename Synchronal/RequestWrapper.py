# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

import json

from PyQt5.QtCore import *
from PyQt5.QtNetwork import *

from .JsonParse import read_json
from .Debug import debug

class RequestWrapper(QObject):
    """Wrapper for a network request which parses output JSON and cleans up after itself."""
    finished = pyqtSignal(object)
    def __init__(self, req, reqs):
        QObject.__init__(self)

        self.req = req
        self.reqs = reqs
        self.req.finished.connect(self.read)
        self.req.sslErrors.connect(self.ssl_error)
        self.reqs.append(self)

    def read(self):
        self.reqs.remove(self)
        if self.req.error() != QNetworkReply.NoError:
            debug("Network error:", self.req.error())
            err = self.req.readAll().data().decode("utf8")
            debug(err)
            if err:
                err = json.loads(err)
                if err["errors"][0]["code"] == 89:
                    # invalid token
                    # giant hack: just blow away the token from settings and wait for
                    # the user to restart the app
                    QSettings().remove("twitter")
                    debug("Deleted Twitter tokens, try again")
            else:
                debug("empty error message body")
            self.finished.emit(None)
            return
        reply = read_json(self.req)
        self.finished.emit(reply)

    def ssl_error(self, errs):
        debug("Got SSL errors:")
        for err in errs:
            debug(err.errorString())

    def callback(self, target):
        # convenience method for connecting to the finished signal
        return self.finished.connect(target)
