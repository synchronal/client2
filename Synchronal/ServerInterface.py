# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

from urllib.parse import urlencode
import re

from PyQt5.QtCore import *
from PyQt5.QtNetwork import *

from .JsonParse import read_json
from .Debug import debug
from .RequestWrapper import RequestWrapper
from .RequestHeaders import RequestHeaders

class ServerInterface(QObject):
    """An interface to the Synchronal server."""
    ready = pyqtSignal(int)
    def __init__(self):
        QObject.__init__(self)

        self.manager = QNetworkAccessManager(self)
        self.api = RequestHeaders(QUrl("https://sentynel.com/synchronal/"), True)
        self.is_ready = False
        self.requests = []

        settings = QSettings()
        self.username = settings.value("synchronal/username", "")
        self.password = settings.value("synchronal/password", "")

    def has_login(self):
        if (not self.username) | (not self.password):
            return False
        if not self.check_login():
            return False
        return True

    @debug
    def set_login(self, username, password):
        if (not username) | (not password):
            return False
        if not self.check_login(username):
            return False
        self.username = username
        self.password = password
        settings = QSettings()
        settings.setValue("synchronal/username", username)
        settings.setValue("synchronal/password", password)
        return True

    def check_login(self, username=None):
        if username is None:
            username = self.username
        if len(username) > 30:
            return False
        if re.search(r"[^\w@+.\-]", username) is not None:
            return False
        return True

    @debug
    def login(self):
        if not self.has_login():
            self.ready.emit(False)
            return False

        self.login_req = self.manager.post(self.api, urlencode({
            "action":"login",
            "user":self.username,
            "pass":self.password}).encode("ascii"))
        self.login_req.finished.connect(self.login_replied)

    @debug
    def login_replied(self):
        if self.login_req.error() != QNetworkReply.NoError:
            debug("Request error:", self.login_req.error())
            self.ready.emit(5)
            return

        reply = read_json(self.login_req)
        debug("Code:", reply.code)
        if reply.code == 0:
            # success
            self.ready.emit(0)
            self.is_ready = True
            return

        self.ready.emit(reply.code)

    @debug
    def register(self):
        if not self.has_login():
            self.ready.emit(2)
            return False

        self.register_req = self.manager.post(self.api, urlencode({
            "action":"register",
            "user":self.username,
            "pass":self.password}).encode("ascii"))
        self.register_req.finished.connect(self.register_replied)

    @debug
    def register_replied(self):
        if self.register_req.error() != QNetworkReply.NoError:
            debug("Request error:", self.register_req.error())
            self.ready.emit(5)
            return

        reply = read_json(self.register_req)
        debug("Code:", reply.code)
        if reply.code == 0:
            self.login()
            return

        self.ready.emit(reply.code)

    @debug
    def get(self, stream, callback=None):
        if len(stream) > 100:
            return False

        req = self.manager.post(self.api, urlencode({
            "action":"get",
            "stream":stream}).encode("ascii"))
        wrapper = RequestWrapper(req, self.requests)
        if callback:
            wrapper.finished.connect(callback)
        return wrapper

    def set(self, stream, tid):
        if len(stream) > 100:
            return False

        tid = int(tid)
        if (tid < 0) | (tid > 2**64):
            return False

        req = self.manager.post(self.api, urlencode({
            "action":"set",
            "stream":stream,
            "tid":tid}).encode("ascii"))
        return req

    def __repr__(self):
        return "Synchronal(username={!r})".format(self.username)


# these are the codes emitted by the ServerInterface.ready() signal
# based on the codes returned by the server itself
# since exceptions can't be raised & handled correctly over a signal connection
return_codes = {
    0: "Success",
    1: "Request error",
    2: "Authentication error",
    3: "Server error",
    4: "Already authenticated",
    5: "Network error",
    11: "Invalid username or password",
    12: "Username taken",
    13: "Invalid login",
}
