# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

#

from PyQt5.QtCore import pyqtSignal, QUrl
try:
    from PyQt5.QtWebEngineWidgets import QWebEngineView as QWebView, QWebEnginePage as QWebPage
    webengine = True
except ImportError:
    from PyQt5.QtWebKitWidgets import QWebView, QWebPage
    webengine = False

#

class TwitterLoginView(QWebView):
    """WebView for handling the browser step of Twitter login"""
    oauth = pyqtSignal(QUrl)
    def __init__(self, *args, **kwargs):
        QWebView.__init__(self, *args, **kwargs)

        self.setPage(NavigationWebPage(self))
        self.page().oauth_url.connect(self.oauth_url)

        self.loadFinished.connect(self.show)

    def oauth_url(self, url):
        self.oauth.emit(url)
        self.hide()
        self.deleteLater()


if not webengine:
    class NavigationWebPage(QWebPage):
        oauth_url = pyqtSignal(QUrl)
        def acceptNavigationRequest(self, frame, request, type):
            if request.url().scheme() == "synchronal-oauth":
                self.oauth_url.emit(request.url())
                return False
            return True
else:
    class NavigationWebPage(QWebPage):
        oauth_url = pyqtSignal(QUrl)
        def acceptNavigationRequest(self, url, type, isMainFrame):
            if url.scheme() == "synchronal-oauth":
                self.oauth_url.emit(url)
                return False
            return True
