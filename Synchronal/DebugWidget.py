# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

#

from PyQt5.QtWidgets import QTextBrowser

from .Debug import debug

class Debug_Widget(QTextBrowser):
    def __init__(self, *args):
        QTextBrowser.__init__(self, *args)
        self.setWindowTitle("Synchronal - Debug")
        self.setLineWrapMode(QTextBrowser.NoWrap)
        self.setFontFamily("mono")
        self.append("".join(debug.log))
        debug.updated.connect(self.append)

_debug_widget = None
def debug_widget():
    global _debug_widget
    if _debug_widget is None:
        _debug_widget = Debug_Widget()
    _debug_widget.show()
