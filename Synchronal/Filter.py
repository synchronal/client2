# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import inspect
import re
import traceback

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from .Debug import debug

class Filter(QObject):
    """Don't display some tweets."""
    @debug
    def __init__(self, params=None):
        self.functions = {}
        if not isinstance(params, str):
            params = None
        if params:
            self.text = params
            try:
                self.compile(self.text, self.functions)
            except Exception as e:
                debug("Error compiling filter:\n" + traceback.format_exc())
    
    @staticmethod
    def compile(data, ns):
        # super unsafe etc - not pretending this is ordinary user friendly any more
        exec(data, globals(), ns)
        for k, v in ns.copy().items():
            if not inspect.isfunction(v):
                del ns[k]
                continue
            if len(inspect.signature(v).parameters) != 1:
                raise ValueError("functions must take 1 argument")

    def filter(self, tweet):
        for k, v in self.functions.items():
            try:
                if not v(tweet):
                    return None
            except Exception as e:
                debug("error in filter", k, "\n", traceback.format_exc())
        return tweet


class FilterDialog(QDialog):
    @debug
    def __init__(self, parent=None, filter=None):
        super().__init__(parent)

        outerlayout = QVBoxLayout()
        self.setLayout(outerlayout)
        self.editor = QPlainTextEdit()
        outerlayout.addWidget(self.editor)
        font = self.editor.document().defaultFont()
        font.setFamily("Courier New")
        self.editor.document().setDefaultFont(font)
        if filter:
            self.editor.setPlainText(filter.text)
        self.buttons = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Apply | QDialogButtonBox.Cancel)
        outerlayout.addWidget(self.buttons)
        self.buttons.clicked.connect(self.button_clicked)

    def button_clicked(self, button):
        role = self.buttons.standardButton(button)
        if role == QDialogButtonBox.Ok:
            if self.save():
                self.accept()
        elif role == QDialogButtonBox.Apply:
            self.save()
        elif role == QDialogButtonBox.Cancel:
            self.reject()

    got_filter = pyqtSignal(object)
    @debug
    def save(self):
        filter = self.editor.toPlainText()
        try:
            Filter.compile(filter, {})
        except Exception as e:
            QMessageBox.critical(self, "Compile failed", traceback.format_exc())
            return False
        else:
            self.got_filter.emit(Filter(filter))
            return True
