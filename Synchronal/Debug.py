# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
import sys
from traceback import format_exception
import inspect
from textwrap import TextWrapper
try: from shutil import get_terminal_size
except ImportError: get_terminal_size = None

from PyQt5.QtCore import pyqtSignal, QObject
from decorator import decorator

#

debug_enabled = True

class Debug(QObject):
    current_indent = 0
    log = []
    updated = pyqtSignal(str)
    def __init__(self):
        QObject.__init__(self)
        self.wrapper = TextWrapper()
        if debug_enabled:
            sys.excepthook = self.excepthook

    def __call__(self, *args, **kwargs):
        if len(args) == 1:
            if inspect.isfunction(args[0]):
                # functioning as decorator
                return self.dbg_wrapper(args[0])
        # functioning as message printer
        self.dbg_msg(*args, **kwargs)

    def dbg_msg(self, *args, **kwargs):
        # kwargs is only used to allow indent to be enabled or disabled
        # and to specify logging but not printing
        # other kwargs are IGNORED
        if debug_enabled:
            indent = self.current_indent * " "
            if indent in kwargs:
                if not kwargs["indent"]:
                    indent = ""
            msg = datetime.now().strftime("[%H:%M:%S]") + indent + " ".join(str(i) for i in args) + "\n"
            if "silent" not in kwargs:
                self.wrapper.width = get_terminal_size((100,24))[0]
                # timestamp is 10 chars wide
                self.wrapper.subsequent_indent = (10 + self.current_indent) * " "
                wrappedmsg = self.wrapper.fill(msg) + "\n"
                try:
                    if sys.stderr is not None:
                        sys.stderr.write(wrappedmsg)
                except IOError:
                    # this happens occasionally for Windows GUI
                    pass
            try:
                self.updated.emit(msg)
            except RuntimeError:
                # this usually occurs on shutdown, as things are deleted
                # just ignore it and proceed with the Python bits
                pass
            self.log.append(msg)

    def _dbg_wrapper(self, fn, *args, **kwargs):
        # this wrapper expects self arguments to be called self
        # behaviour may be odd if they're not.

        # get function name, prepare class name (will be set later)
        fnname = fn.__name__
        classname = ""

        # work out call line
        call = inspect.getcallargs(fn, *args, **kwargs)
        argspec = inspect.getargspec(fn)
        callargs = argspec[0]
        for i in (argspec.varargs, argspec.keywords):
            if i is not None:
                callargs.append(i)
        pretty_args = []
        for i in argspec[0]:
            if i != "self":
                arg = repr(call[i])
                if len(arg) > 40:
                    arg = arg[:37] + "..."
                pretty_args.append(i + "=" + arg)
            else:
                # set class name
                classname = call[i].__class__.__name__ + "."
        call = ", ".join(pretty_args)

        self.dbg_msg("START:{}{}({})".format(classname, fnname, call))
        self.current_indent += 1
        res = fn(*args, **kwargs)
        if self.current_indent:
            self.current_indent -= 1
        self.dbg_msg("END__:{}{}()".format(classname, fnname))
        return res

    def dbg_wrapper(self, f):
        return decorator(self._dbg_wrapper, f)

    def excepthook(self, t, v, e):
        self.current_indent = 0
        self.dbg_msg("\n" + "".join(format_exception(t, v, e)), silent=True, indent=False)
        sys.__excepthook__(t, v, e)

if get_terminal_size is None:
    # this function is added in Python 3.3
    # if it's not available, just use the fallback size from that
    def get_terminal_size(fallback=(80, 24)):
        return fallback

debug = Debug()
