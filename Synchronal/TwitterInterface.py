# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

from time import time
from uuid import uuid4
from urllib.parse import urlencode, quote
from hashlib import sha1
from base64 import b64encode
from hmac import new as hmac
from random import randrange
import zlib
import re

from PyQt5.QtCore import *
from PyQt5.QtNetwork import *
from decorator import FunctionMaker

from .JsonParse import json_byte_array
from .Debug import debug
from .RequestWrapper import RequestWrapper
from .RequestHeaders import RequestHeaders

class OAuthInterface(QObject):
    """An object for handling OAuth tokens and signing requests."""
    def __init__(self, key, secret):
        QObject.__init__(self)

        self.consumer_key = key
        self.consumer_secret = secret

        settings = QSettings()
        self.access_token = str(settings.value("twitter/token", ""))
        self.access_secret = str(settings.value("twitter/secret", ""))

    def set_access_token(self, token, secret):
        self.access_token = str(token)
        self.access_secret = str(secret)
        settings = QSettings()
        settings.setValue("twitter/token", token)
        settings.setValue("twitter/secret", secret)

    def has_token(self):
        if (self.access_token != "") & (self.access_secret != ""):
            return True
        return False

    access_token_available = pyqtSignal(str, str)
    access_token_error = pyqtSignal(str, str)

    def get_access_token(self):
        # starts the process of retrieving an access token
        self.oauth_path = "https://api.twitter.com/oauth/"
        self.manager = QNetworkAccessManager(self)
        req = RequestHeaders(QUrl(self.oauth_path + "request_token"), True)
        params = {"oauth_callback":"synchronal-oauth://got-verifier"}
        self.authorize(req, params)
        self.request_token_req = self.manager.post(req, urlencode(params).encode("ascii"))
        self.request_token_req.finished.connect(self.got_request_token)

    # this signal should be connected and handled by a web browser widget of some sort
    # and redirects to synchronal-oauth:// URLs passed back to got_request_verifier
    request_token_url = pyqtSignal(QUrl)
    def got_request_token(self):
        if self.request_token_req.error() != QNetworkReply.NoError:
            err = str(self.request_token_req.error())
            data = self.request_token_req.readAll().data().decode("utf8")
            self.access_token_error.emit(err, data)
            debug("Network error:", err)
            debug(data)
            return

        res = self.request_token_req.readAll().data().decode("utf8")
        del self.request_token_req
        res = dict(i.split("=") for i in res.split("&"))
        if res["oauth_callback_confirmed"] != "true":
            self.access_token_error.emit("OAuth error", str(res))
            return
        self.request_token = res["oauth_token"]
        self.request_secret = res["oauth_token_secret"]
        url = QUrl(self.oauth_path + "authorize")
        query = QUrlQuery(url)
        query.addQueryItem("oauth_token", self.request_token)
        url.setQuery(query)
        self.request_token_url.emit(url)

    def got_request_verifier(self, url):
        result = dict(QUrlQuery(url).queryItems())
        if result["oauth_token"] != self.request_token:
            self.access_token_error.emit("OAuth error", "oauth_token is wrong")
            return
        request_verifier = result["oauth_verifier"]
        req = RequestHeaders(QUrl(self.oauth_path + "access_token"), True)
        params = {"oauth_verifier":request_verifier}
        self.authorize(req, params)
        self.access_token_req = self.manager.post(req, urlencode(params).encode("ascii"))
        self.access_token_req.finished.connect(self.got_access_token)

    def got_access_token(self):
        del self.request_token, self.request_secret
        if self.access_token_req.error() != QNetworkReply.NoError:
            err = str(self.access_token_req.error())
            data = self.access_token_req.readAll()
            self.access_token_error.emit(err, data)
            debug("Network error:", err)
            debug(data)
            return

        res = self.access_token_req.readAll().data().decode("utf8")
        del self.access_token_req, self.manager
        res = dict(i.split("=") for i in res.split("&"))

        token, secret = res["oauth_token"], res["oauth_token_secret"]
        self.set_access_token(token, secret)
        self.access_token_available.emit(token, secret)

    def authorize(self, request, parameters=None):
        oauth_dict = {
            "oauth_version":"1.0",
            "oauth_signature_method":"HMAC-SHA1",
            "oauth_timestamp":str(int(time())),
            "oauth_nonce":uuid4().hex,
            "oauth_consumer_key":self.consumer_key,
            }
        if self.access_token:
            oauth_dict["oauth_token"] = self.access_token
        else:
            try:
                oauth_dict["oauth_token"] = self.request_token
            except AttributeError:
                pass
        oauth_dict["oauth_signature"] = self.sign(request, parameters, oauth_dict)

        header = b"OAuth " + ", ".join('{}="{}"'.format(quote(key, ""), quote(val, "")) for key, val in sorted(oauth_dict.items())).encode("ascii")
        request.setRawHeader(b"Authorization", header)

    def sign(self, request, parameters, oauth):
        items = []
        if parameters is None:
            items.append("GET")
        else:
            items.append("POST")
        items.append(request.url().toString(QUrl.RemoveQuery | QUrl.RemoveFragment))
        req_params = {}
        if parameters:
            req_params.update(parameters)
        req_params.update(oauth)
        req_params.update(dict(QUrlQuery(request.url()).queryItems()))
        # urlencode incorrectly uses + rather than %20 for space, so do this manually
        param_string = "&".join("{}={}".format(quote(key, ""), quote(value, "")) for key, value in sorted(req_params.items()))
        items.append(param_string)
        base_string = "&".join(quote(i, "") for i in items)
        if self.access_secret:
            key = self.access_secret
        else:
            try:
                key = self.request_secret
            except AttributeError:
                key = ""
        signing_key = self.consumer_secret + "&" + key
        return b64encode(hmac(signing_key.encode("utf8"), base_string.encode("utf8"), sha1).digest())


class TwitterInterface(QObject):
    """An interface to Twitter."""
    def _bind(self, request_method, api_method, url_id=False, required_params=[], optional_params=[]):
        # actual method which will be called
        realmeth = ApiMethod(self, request_method, api_method, url_id, required_params, optional_params)
        # wrap method in a function with the correct signature
        path = api_method.split("/")
        fname = path[-1]
        if not url_id:
            params = required_params + optional_params
        else:
            params = ["id"] + required_params + optional_params
        fsig = ", ".join(params)
        defaults = (None,) * len(optional_params)
        meth = FunctionMaker.create("{}({})".format(fname, fsig), "return realmeth({})".format(fsig), {"realmeth":realmeth}, defaults=defaults)
        # debug decorator
        meth = debug(meth)

        # place method in correct place by API name
        i = self
        for item in path[:-1]:
            try:
                i = getattr(i, item)
            except AttributeError:
                setattr(i, item, Holder())
                i = getattr(i, item)
        setattr(i, path[-1], meth)

    def __init__(self, key, secret):
        QObject.__init__(self)
        self.oauth = OAuthInterface(key, secret)
        self.manager = QNetworkAccessManager(self)
        self.requests = []

        methods = [
            ("GET", "statuses/home_timeline", False, [], ["count", "since_id", "max_id",  "trim_user", "exclude_replies", "contributor_details", "include_entities", "tweet_mode"]),
            ("GET", "statuses/mentions_timeline", False, [], ["count", "since_id", "max_id",  "trim_user", "contributor_details", "include_entities", "tweet_mode"]),
            ("GET", "statuses/show", True, [], ["trim_user", "include_my_retweet", "include_entities", "tweet_mode"]),
            ("POST", "statuses/update", False, ["status"], ["in_reply_to_status_id", "lat", "long", "place_id", "display_coordinates", "trim_user"]),
            ("GET", "search/tweets", False, ["q"], ["geocode", "lang", "locale", "result_type", "count", "until", "since_id", "max_id", "include_entities", "callback", "tweet_mode"]),
            ("POST", "favorites/create", False, ["id"], ["include_entities"]),
            ("POST", "favorites/destroy", False, ["id"], ["include_entities"]),
            ("POST", "statuses/retweet", True, [], ["trim_user"]),
            ("POST", "statuses/unretweet", True, [], ["trim_user"]),
            ]

        for i in methods:
            self._bind(*i)

    def __repr__(self):
        return "TwitterInterface()"


class ApiMethod(QObject):
    """A standard method on the Twitter API"""
    def __init__(self, parent, request_method, api_method, url_id=False, required_params=[], optional_params=[], api_root="https://api.twitter.com/1.1/"):
        QObject.__init__(self)

        self.api_root = api_root
        self.api_method = api_method
        self.url_id = url_id
        self.method = request_method
        if url_id:
            self.params = ["id"] + required_params + optional_params
        else:
            self.params = required_params + optional_params
        self.parent = parent

    def __call__(self, *args):
        # we assume that the call here is correct
        # TwitterInterface._bind should be used to get a callable with the correct
        # signature, since __call__ can't be dynamically redefined
        liveparams = [i for i, arg in enumerate(args) if arg is not None]
        params = dict(zip([self.params[i] for i in liveparams], [args[i] for i in liveparams]))

        if not self.url_id:
            url = QUrl(self.api_root + self.api_method + ".json")
        else:
            url = QUrl(self.api_root + self.api_method + "/" + str(params["id"]) + ".json")
            del params["id"]
        # now run actual request
        if self.method == "GET":
            # args in url string
            query = QUrlQuery(url)
            query.setQueryItems(list((str(i), str(j)) for i, j in params.items()))
            url.setQuery(query)
            params = None
        req = RequestHeaders(url, self.method != "GET")
        self.parent.oauth.authorize(req, params)
        if self.method == "GET":
            res = self.parent.manager.get(req)
        else:
            res = self.parent.manager.post(req, urlencode(params).encode("ascii"))
        return RequestWrapper(res, self.parent.requests)


class Holder(object):
    """A simple holder object for paths in the API."""
