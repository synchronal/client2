# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

from collections import Counter, deque
from math import ceil

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from .Debug import debug
from .Tweet import Tweet

class Column(QScrollArea):
    def __init__(self, window):
        QScrollArea.__init__(self)
        self.window = window

        self.container = QWidget()
        self.container_layout = QVBoxLayout()
        self.container_layout.addStretch()
        self.container.setLayout(self.container_layout)
        self.container.show()
        self.setWidget(self.container)
        self.setWidgetResizable(True)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        # newer tweets at higher list indices
        # drawn with newer tweets at the top
        self.tweets = []

    def add_tweet(self, tweet):
        if not isinstance(tweet, QWidget):
            tweet = Tweet(tweet, self.window)
        self.tweets.append(tweet)
        self.container_layout.insertWidget(0, tweet)

    def remove_tweet(self, tweet):
        # replace tweet with an indicator that something was removed
        for i, t in enumerate(self.tweets):
            if t.tweet.id_str == tweet:
                break
        else:
            return False
        self.container_layout.removeWidget(t)
        del self.tweets[i]
        l = QLabel("<strong>{}</strong> ({})".format(t.tweet.user.screen_name, t.tweet.user.name))
        l.setContentsMargins(t.default_margins[0])
        l.setEnabled(False)
        self.container_layout.insertWidget(len(self.tweets) - i, l)
        return t


class OrderedColumn(Column):
    def __init__(self, window):
        Column.__init__(self, window)
        # newer tweets at lower indices/left of deque
        # drawn with newer tweets at the bottom
        self.tweets = deque()
        self.setVerticalScrollBar(DisabledScrollBar(self))

    def add_tweet(self, tweet):
        tweet = Tweet(tweet, self.window)
        if not self.tweets:
            # first tweet
            pos = 0
            self.tweets.append(tweet)
            tweet.toggle_expand()
        elif tweet >= self.tweets[0]:
            # newer than newest
            pos = len(self.tweets)
            self.tweets.appendleft(tweet)
        elif tweet < self.tweets[-1]:
            # older than oldest
            pos = 0
            if self.tweets[-1].expanded:
                self.tweets[-1].toggle_expand()
                tweet.toggle_expand()
            self.tweets.append(tweet)
        else:
            # doesn't fit neatly on either end, must rotate deque and insert
            # this is most likely a new tweet that arrived slightly out of order
            rotation = 0
            maxrotate = len(self.tweets)
            while tweet < self.tweets[0]:
                self.tweets.rotate(-1)
                rotation += 1
                if rotation >= maxrotate:
                    debug("Column.add_tweet: Insertion failure")
                    debug(tweet.tweet.created_at)
                    debug([i.tweet.created_at for i in self.tweets])
                    return
            pos = len(self.tweets) - rotation
            self.tweets.appendleft(tweet)
            self.tweets.rotate(rotation)
        self.container_layout.insertWidget(pos, tweet)

        tweets_l = [i.tweet.created_at for i in self.tweets]
        tweets_s = sorted(tweets_l, reverse=True)
        if tweets_l != tweets_s:
            for t in self.tweets:
                debug(t.tweet.created_at, t.tweet.in_reply_to_status_id_str, t.tweet.id_str)
            QMessageBox.warning(self, "Synchronal", "Tweets are out of order: timestamps", QMessageBox.Ok, QMessageBox.Ok)
        seen_tweets = set()
        for t in self.tweets:
            # newer tweets first, so we should never see a tweet replying to a tweet we've already seen
            seen_tweets.add(t.tweet.id_str)
            if t.tweet.in_reply_to_status_id_str in seen_tweets:
                debug("Problematic tweet: {}", t.tweet.id_str)
                for t in self.tweets:
                    debug(t.tweet.created_at, t.tweet.in_reply_to_status_id_str, t.tweet.id_str)
                QMessageBox.warning(self, "Synchronal", "Tweets are out of order: reply to newer tweet", QMessageBox.Ok, QMessageBox.Ok)
        seen_reply_tos = set()
        for t in reversed(self.tweets):
            if t.tweet.in_reply_to_status_id:
                seen_reply_tos.add(t.tweet.in_reply_to_status_id)
            if t.tweet.id_str in seen_reply_tos:
                debug("Problematic tweet: {}", t.tweet.id_str)
                for t in self.tweets:
                    debug(t.tweet.created_at, t.tweet.in_reply_to_status_id_str, t.tweet.id_str)
                QMessageBox.warning(self, "Synchronal", "Tweets are out of order: older tweet replies to this one", QMessageBox.Ok, QMessageBox.Ok)

    def remove_tweet(self, tweet=None):
        if not self.tweets:
            return None
        if tweet is None:
            # remove and return oldest tweet
            tweet = self.tweets.pop()
        else:
            rotate = 0
            maxrotate = len(self.tweets)
            while self.tweets[0].tweet.id_str != tweet:
                self.tweets.rotate(-1)
                rotate += 1
                if rotate >= maxrotate:
                    return False
            tweet = self.tweets.popleft()
            self.tweets.rotate(rotate)
        self.container_layout.removeWidget(tweet)
        return tweet

    read_tweets = pyqtSignal(int)
    def keyPressEvent(self, event):
        # TODO make PageDown mark each visible tweet as read
        if event.key() in (Qt.Key_Down, Qt.Key_J, Qt.Key_PageDown):
            self.read_tweets.emit(1)
            return
        if not self.tweets:
            return Column.keyPressEvent(self, event)
        if event.key() == Qt.Key_O:
            # expand/contract top tweet
            self.tweets[-1].toggle_expand()
            return
        if event.key() == Qt.Key_V:
            # open quick link in top tweet if present
            if self.tweets[-1].quick_link:
                QDesktopServices.openUrl(QUrl(self.tweets[-1].quick_link[0]))
            return
        return Column.keyPressEvent(self, event)

    def wheelEvent(self, event):
        delta = event.angleDelta().y()
        if delta < 0:
            # standard mouse wheel rotation click is 15 degrees
            # delta is in eighths of a degree
            # any rotation towards user (-ve) should scroll down (for users
            # with finer mouse wheels)
            steps = ceil(-delta/120)
            self.read_tweets.emit(steps)
        event.accept()


class DisabledScrollBar(QScrollBar):
    """A disabled scrollbar.

    Displayed for information, but can't be used to control the scrollarea.
    Simply passes the relevant events up to the scrollarea for handling."""
    def __init__(self, scroll_area, *args, **kwargs):
        QScrollBar.__init__(self, *args, **kwargs)
        self.setEnabled(False)
        self.scroll_area = scroll_area

    def event(self, event):
        if event.type() == QEvent.Wheel:
            self.scroll_area.wheelEvent(event)
            return False
        else:
            return QScrollBar.event(self, event)
