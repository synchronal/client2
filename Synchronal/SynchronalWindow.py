# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

import json

from PyQt5.QtCore import *
from PyQt5.QtNetwork import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from .Debug import debug
from .DebugWidget import debug_widget
from .Filter import Filter, FilterDialog
from .ServerInterface import ServerInterface
from .TwitterInterface import TwitterInterface
from .StreamView import StreamView
from .StreamController import StreamController
from .Tweet import Stats
from .Tweeter import Tweeter
from .TwitterLoginView import TwitterLoginView

SHORTCUTS = """Shortcuts:

Down/j/PgDn: Mark top tweet as read
o: Expand/contract top tweet
v: Open link from top tweet
Ctrl+Q: Quit
"""

class SynchronalWindow(QMainWindow):
    @debug
    def __init__(self, *args):
        QMainWindow.__init__(self, *args)

        app = QApplication.instance()
        app.setOrganizationName("Sentynel")
        app.setApplicationName("Synchronal")
        self.update_title()

        self.load_resources()

        # load bundled ca.crt to avoid SSL errors on Windows
        QSslSocket.addDefaultCaCertificate(self.ca_crt)

        self.synchronal = ServerInterface()
        self.twitter = TwitterInterface(self.keys["key"], self.keys["secret"])

        app.setWindowIcon(self.synchronal_logo)

        geometry = QSettings().value("window/geometry")
        if geometry:
            self.restoreGeometry(geometry)
        else:
            self.resize(500,300)

        loadfilter = QSettings().value("filter/params")
        if loadfilter:
            self.filter = Filter(loadfilter)
        else:
            self.filter = None

        quit_action = QAction("Quit", self)
        quit_action.setShortcut(QKeySequence("Ctrl+Q"))
        quit_action.triggered.connect(self.close)
        self.addAction(quit_action)

        self.create_widgets()

        self.check_twitter_login()

    @debug
    def load_resources(self):
        #TODO don't require starting in correct directory
        self.synchronal_logo = QIcon("resources/synchronal_logo.svg")
        with open("resources/startcom_ca.crt", "rb") as f:
            self.ca_crt = QSslCertificate(f.read(), QSsl.Der)
        with open("resources/keys.json") as f:
            self.keys = json.load(f)
        QFontDatabase.addApplicationFont("resources/Font Awesome 5 Free-Solid-900.otf")

    @debug
    def create_widgets(self):
        main = QWidget()
        self.setCentralWidget(main)
        main_layout = QVBoxLayout()
        main.setLayout(main_layout)

        tweeter = Tweeter(self.twitter)
        main_layout.addWidget(tweeter)

        self.stream_view = StreamView(self)
        main_layout.addWidget(self.stream_view)
        self.stream_view.unread_signal.connect(self.update_title)

        menubar = self.menuBar()
        menu = menubar.addMenu("Synchronal")
        filteraction = menu.addAction("Filter")
        filteraction.triggered.connect(self.show_filter)
        debugaction = menu.addAction("Debug")
        debugaction.triggered.connect(self.show_debug)
        statsaction = menu.addAction("Stats")
        statsaction.triggered.connect(self.show_stats)

        help_menu = menubar.addMenu("Help")
        shortcutsaction = help_menu.addAction("Shortcuts")
        shortcutsaction.triggered.connect(self.show_shortcuts)

    def check_twitter_login(self):
        if not self.twitter.oauth.has_token():
            self.get_twitter_login()
        else:
            self.begin_streaming()

    @debug
    def begin_streaming(self):
        self.controller = StreamController(self.synchronal, self.twitter, self, self.filter)
        # connect up signals
        self.controller.got_tweet.connect(self.stream_view.add_tweet)
        self.controller.remove_tweet.connect(self.stream_view.remove_tweet)
        self.stream_view.read_tweet.connect(self.controller.read_tweet)

    @debug
    def get_twitter_login(self):
        self.login_view = TwitterLoginView()
        self.twitter.oauth.request_token_url.connect(self.login_view.load)
        self.login_view.oauth.connect(self.twitter.oauth.got_request_verifier)
        self.twitter.oauth.access_token_available.connect(self.got_twitter_login)
        self.twitter.oauth.access_token_error.connect(self.twitter_login_failed)
        self.twitter.oauth.get_access_token()

    @debug
    def got_twitter_login(self, token, secret):
        # save tokens
        settings = QSettings()
        settings.setValue("twitter/access_token", token)
        settings.setValue("twitter/access_secret", secret)
        self.begin_streaming()

    @debug
    def twitter_login_failed(self, err, msg):
        QMessageBox.critical(self, "Synchronal", err + ": " + msg)
        self.twitter.oauth.access_token_available.disconnect()
        self.twitter.oauth.access_token_error.disconnect()
        self.get_twitter_login()

    @debug
    def closeEvent(self, event):
        if hasattr(self, "controller"):
            self.controller.stop()
        QSettings().setValue("window/geometry", self.saveGeometry())
        self.save_filter()
        event.accept()

    def update_title(self, count=0):
        self.setWindowTitle("Synchronal - {}".format(count))

    @debug
    def show_filter(self):
        self.filter_dialog = FilterDialog(self, self.filter)
        self.filter_dialog.got_filter.connect(self.set_filter)
        self.filter_dialog.show()

    @debug
    def show_debug(self):
        debug_widget()

    @debug
    def set_filter(self, filter):
        self.filter = filter
        self.controller.filter = filter
        self.save_filter()

    @debug
    def save_filter(self):
        if self.filter and self.filter.text:
            QSettings().setValue("filter/params", self.filter.text)

    @debug
    def show_stats(self):
        self.stats_window = Stats()
        self.stats_window.show()

    @debug
    def show_shortcuts(self):
        QMessageBox.information(self, "Synchronal Shortcuts", SHORTCUTS,
                QMessageBox.Ok)
