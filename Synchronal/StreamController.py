# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

#

from PyQt5.QtCore import *

from .Debug import debug
from .ServerLoginDialog import ServerLoginDialog

class StreamController(QObject):
    got_tweet = pyqtSignal(object)
    remove_tweet = pyqtSignal(str)
    @debug
    def __init__(self, synchronal, twitter, dialog_parent=None, filter=None):
        QObject.__init__(self)
        self.synchronal = synchronal
        self.twitter = twitter
        self.dialog_parent = dialog_parent
        self.filter = filter
        self.tweets = set()

        # mechanisms to ensure the last read id is only ever increased
        # and that we don't spam the server with requests
        self.setting_read = None
        self.last_set_read = 0
        self.set_read_timer = QTimer()
        self.set_read_timer.setSingleShot(True)
        self.set_read_timer.setInterval(5*1000)
        self.set_read_timer.timeout.connect(self.set_read)

        # tracking for filling in around stream problems/backfilling
        self.received_oldest = None
        # n.b. 0 is an invalid value here - it's fixed when we get a response
        # from Synchronal, or failing that when we just get the last batch of
        # tweets, but in the case where there are no tweets to get and then we
        # follow a bunch of people, we'll need a client restart to actually
        # start getting tweets. not much I can do about this I don't think
        self.received_newest = 0
        self.synchronal_tid = None
        self.poll_timer = QTimer()
        self.poll_timer.timeout.connect(self.poll_new_tweets)
        self.poll_timer.setInterval(1000 * 60) # poll every minute

        # login to Synchronal
        # TODO store login cookies in QSettings to skip this step
        if self.synchronal.has_login():
            self.synchronal.login()
        else:
            self.get_login_details()
        self.synchronal.ready.connect(self.got_login)

    @debug
    def get_login_details(self):
        self.login_dialog = ServerLoginDialog(self.dialog_parent)
        self.login_dialog.finished.connect(self.got_login_details)

    @debug
    def got_login_details(self, accepted):
        if not accepted:
            # just get most recent 20
            self.twitter.statuses.home_timeline(tweet_mode="extended").callback(self.backfill)
            return
        if self.synchronal.set_login(self.login_dialog.username.text(), self.login_dialog.password.text()):
            self.synchronal.login()
        else:
            QMessageBox.warning(self.dialog_parent, "Synchronal", "Username or password not valid. Please try again.")
            self.get_login_details()

    @debug
    def got_login(self, code):
        if code != 0:
            debug("Error:", code)
            return
        # pull starting TID
        self.synchronal.get("__main__").callback(self.got_tid)

    @debug
    def got_tid(self, reply):
        if "tid" not in reply:
            debug(reply)
            return
        self.synchronal_tid = tid = reply.tid
        if tid == "0":
            # no saved tid available, just get last 20
            self.twitter.statuses.home_timeline(tweet_mode="extended").callback(self.backfill)
        else:
            self.received_newest = int(tid)
            # get home timeline and mentions from given tid
            self.twitter.statuses.home_timeline(since_id=tid, count=200, tweet_mode="extended").callback(self.backfill_home)
            self.twitter.statuses.mentions_timeline(since_id=tid, count=200, tweet_mode="extended").callback(self.backfill_mentions)
        self.poll_timer.start()

    def _got_tweet(self, tweet):
        if tweet.id in self.tweets:
            # discard duplicates
            return
        if tweet.id > self.received_newest:
            self.received_newest = tweet.id
        if (not self.received_oldest) or (tweet.id < self.received_oldest):
            self.received_oldest = tweet.id
        if self.filter:
            tweet = self.filter.filter(tweet)
        if tweet:
            self.tweets.add(tweet.id)
            self.got_tweet.emit(tweet)

    @debug # TODO remove
    def poll_new_tweets(self):
        for i in (self.twitter.statuses.home_timeline, self.twitter.statuses.mentions_timeline):
            i(since_id=self.received_newest, count=200, tweet_mode="extended").callback(self.poll_add_tweets)

    @debug
    def poll_add_tweets(self, tweets):
        if not tweets:
            return
        # sort to make appends a bit more efficient
        for i in sorted(tweets, key=lambda x: x.created_at):
            self._got_tweet(i)

    def backfill_home(self, tweets):
        self.backfill(tweets, "home_timeline")
    def backfill_mentions(self, tweets):
        self.backfill(tweets, "mentions")

    @debug
    def backfill(self, tweets, method=None):
        if not tweets:
            return
        for i in tweets:
            self._got_tweet(i)
        if len(tweets) > 100:
            # there might be more older tweets to get (the limit is 200 at once
            # but you don't actually recieve that many)
            debug("Received >100 tweets from method", method)
            oldest = min(i.id for i in tweets)
            if method == "mentions":
                self.twitter.statuses.mentions_timeline(since_id=self.synchronal_tid, max_id=str(oldest), count=200, tweet_mode="extended").callback(self.backfill_mentions)
            else:
                self.twitter.statuses.home_timeline(since_id=self.synchronal_tid, max_id=str(oldest), count=200, tweet_mode="extended").callback(self.backfill_home)

    def read_tweet(self, tid):
        # use integers for comparisons here - python ints are big enough and
        # it's more reliable than string comparisons
        tidint = int(tid)
        if self.last_set_read > tidint:
            return
        self.last_set_read = tidint
        self.setting_read = tid
        self.set_read_timer.start()

    def set_read(self):
        if self.setting_read:
            self.synchronal.set("__main__", self.setting_read)
            self.setting_read = None

    def delete_tweet(self, delete):
        if delete.status.id in self.tweets:
            self.remove_tweet.emit(delete.status.id_str)
        else:
            # add to known tweets to prevent the real tweet being added if/when
            # it arrives
            self.tweets.add(delete.status.id)

    def stop(self):
        self.poll_timer.stop()
