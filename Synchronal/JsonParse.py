# Copyright 2012 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

import json
from datetime import datetime

#

#

class JsonObject(object):
    def __init__(self, dic):
        dic = self.transform(dic)
        self.__dict__.update(dic)

    def transform(self, dic):
        # any operations performed on the dictionary to convert values etc go here
        return dic

    def __getitem__(self, i):
        return getattr(self, i)
    def __contains__(self, item):
        return item in self.__dict__
    def __repr__(self):
        return "JsonObject({{{}}})".format(", ".join('{!r}:{!r}'.format(i, j) for i, j in self.__dict__.items() if not i.startswith("_")))


class TwitterApiObject(JsonObject):
    def transform(self, dic):
        if "created_at" in dic:
            dic["created_at"] = datetime.strptime(dic["created_at"], "%a %b %d %H:%M:%S +0000 %Y")
        return dic


def read_json(reply):
    return json.loads(str(reply.readAll().data(), "utf8"), object_hook=TwitterApiObject)

def json_byte_array(bytearray):
    return json.loads(str(bytearray.data(), "utf8"), object_hook=TwitterApiObject)
